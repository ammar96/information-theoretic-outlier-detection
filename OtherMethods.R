library('HDoutliers')
library('OutlierDM')
library('arules')
library('DescTools')
data(ex2D)
ex2D.dis1 <- discretize(ex2D[, 1], categories = 10)
ex2D.dis2 <- discretize(ex2D[, 2], categories = 10)
ex2D.dis <- as.data.frame(cbind(ex2D.dis1, ex2D.dis2))
allData <- ex2D.dis
allData <- convert.to.categ()
data <- allData
estimated.num.outliers <- 10
remove.outliers(estimated.num.outliers)
plotHDoutliers(ex2D, get.indices(myOutliers))
